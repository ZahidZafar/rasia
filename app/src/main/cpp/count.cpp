#include <jni.h>
#include <string>

jint counter = 0;

extern "C" JNIEXPORT jstring

JNICALL
Java_com_test_zahi_myapplication_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jint

JNICALL
Java_com_test_zahi_myapplication_MainActivity_countFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    ++counter;
    return counter;
}
